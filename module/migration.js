/**
 * Perform a system migration for the entire World, applying migrations for Actors, Items, and Compendium packs
 * @return {Promise}      A Promise which resolves once the migration is completed
 */
export const migrateWorld = async function(version_nums_current, version_nums_migration) {
  ui.notifications.info(`Applying CofD system migration for version ${game.system.data.version}. Please be patient and do not close your game or shut down your server.`, {permanent: true});
  console.log("Migrating to version " + game.data.system.data.version);
  let isError = false;
  
  // Migrate World Actors
  for ( let a of game.actors.contents ) {
    try {
      const updateData = migrateActorData(a.data, version_nums_current, version_nums_migration);
      if ( !isObjectEmpty(updateData) ) {
        console.log(`Migrating Actor entity ${a.name}`);
        await a.update(updateData, {enforceTypes: false});
        if(compareVersion(version_nums_current, [0,6,0])) { // 0.5.0 -> 0.6.0
          if(a.data.type === "character"){
            await a.createWerewolfForms();
          }
        }
      }
    } catch(err) {
      err.message = `Failed CofD system migration for Actor ${a.name}: ${err.message}`;
      console.error(err);
      isError = true;
    }
  }
  
  // Migrate World Items
  for ( let i of game.items.contents ) {
    try {
      const updateData = migrateItemData(i.data, version_nums_current, version_nums_migration);
      if ( !isObjectEmpty(updateData) ) {
        console.log(`Migrating Item entity ${i.name}`);
        await i.update(updateData, {enforceTypes: false});
      }
    } catch(err) {
      err.message = `Failed CofD system migration for Item ${i.name}: ${err.message}`;
      console.error(err);
      isError = true;
    }
  }

  // Migrate Actor Override Tokens
  for ( let s of game.scenes.contents ) {
    try {
      const updateData = migrateSceneData(s.data, version_nums_current, version_nums_migration);
      if ( !isObjectEmpty(updateData) ) {
        console.log(`Migrating Scene entity ${s.name}`);
        await s.update(updateData, {enforceTypes: false});
      }
    } catch(err) {
      err.message = `Failed CofD system migration for Scene ${s.name}: ${err.message}`;
      console.error(err);
      isError = true;
    }
  }

  // Migrate World Compendium Packs
  for ( let p of game.packs ) {
    if ( p.metadata.package !== "world" ) continue;
    if ( !["Actor", "Item", "Scene"].includes(p.metadata.entity) ) continue;
    await migrateCompendium(p, version_nums_current, version_nums_migration);
  }

  // Set the migration as complete
  if(isError) {
    ui.notifications.error(`An error occured during the system migration. Please check the console (F12) for details.`, {permanent: true});
  }
  else {
    game.settings.set("mta", "systemMigrationVersion", game.data.system.data.version);
    console.log("Migration completed!");
    ui.notifications.info(`CofD system migration to version ${game.system.data.version} completed!`, {permanent: true});
  }
};

/**
 * Apply migration rules to all Entities within a single Compendium pack
 * @param pack
 * @return {Promise}
 */
 export const migrateCompendium = async function(pack, version_nums_current, version_nums_migration) {
  const entity = pack.metadata.entity;
  if ( !["Actor", "Item", "Scene"].includes(entity) ) return;

  // Unlock the pack for editing
  const wasLocked = pack.locked;
  await pack.configure({locked: false});

  // Begin by requesting server-side data model migration and get the migrated content
  await pack.migrate();
  const content = await pack.getDocuments();

  // Iterate over compendium entries - applying fine-tuned migration functions
  for ( let ent of content ) {
    let updateData = {};
    try {
      switch (entity) {
        case "Actor":
          updateData = migrateActorData(ent.data, version_nums_current, version_nums_migration);
          break;
        case "Item":
          updateData = migrateItemData(ent.data, version_nums_current, version_nums_migration);
          break;
        case "Scene":
          updateData = migrateSceneData(ent.data, version_nums_current, version_nums_migration);
          break;
      }
      if ( isObjectEmpty(updateData) ) continue;

      // Save the entry, if data was changed
      updateData["_id"] = ent.data._id;
      await ent.update(updateData);
      console.log(`Migrated ${entity} entity ${ent.name} in Compendium ${pack.collection}`);
    }

    // Handle migration failures
    catch(err) {
      err.message = `Failed cofd system migration for entity ${ent.name} in pack ${pack.collection}: ${err.message}`;
      console.error(err);
    }
  }

  // Apply the original locked status for the pack
  pack.configure({locked: wasLocked});
  console.log(`Migrated all ${entity} entities from Compendium ${pack.collection}`);
};


/* -------------------------------------------- */
/*  Entity Type Migration Helpers               */
/* -------------------------------------------- */

/**
 * Migrate a single Actor entity to incorporate latest data model changes
 * Return an Object of updateData to be applied
 * @param {Actor} actor   The actor to Update
 * @return {Object}       The updateData to apply
 */
export const migrateActorData = function(actor, version_nums_current, version_nums_migration) {
  const updateData = {};
  const model = game.system.model.Actor[actor.type];

  updateData.data = _nullToUndefined(actor.data, 5);
  updateData.data = mergeObject(updateData.data,model,{insertKeys: true, insertValues: true, overwrite: false, inplace: false, enforceTypes: false})

  if(compareVersion(version_nums_current, [0,3,0]) && actor.type === "character" && updateData.data.characterType === "Sleeper") updateData.data.characterType = "Mortal"; // 0.2.3 -> 0.3.0
  if(compareVersion(version_nums_current, [0,4,0]) && actor.type === "character" && updateData.data.goblinDebt){ // 0.3.4 -> 0.4.0 in certain edge cases
    if(updateData.data.goblinDebt.max < 10 ) updateData.data.goblinDebt.max = 10;
  }
  if(compareVersion(version_nums_current, [0,6,0])) { // 0.5.0 -> 0.6.0

    if(actor.type === "character"){
      if(updateData.data.attributes?.physical) updateData.data.attributes_physical = duplicate(updateData.data.attributes.physical);
      if(updateData.data.attributes?.social) updateData.data.attributes_social = duplicate(updateData.data.attributes.social);
      if(updateData.data.attributes?.mental) updateData.data.attributes_mental = duplicate(updateData.data.attributes.mental);
      updateData.data['-=attributes'] = null;

      if(updateData.data.skills?.physical) updateData.data.skills_physical = duplicate(updateData.data.skills.physical);
      if(updateData.data.skills?.social) updateData.data.skills_social = duplicate(updateData.data.skills.social);
      if(updateData.data.skills?.mental) updateData.data.skills_mental = duplicate(updateData.data.skills.mental);
      updateData.data['-=skills'] = null;

      if(updateData.data.arcana?.gross) updateData.data.arcana_gross = duplicate(updateData.data.arcana.gross);
      if(updateData.data.arcana?.subtle) updateData.data.arcana_subtle = duplicate(updateData.data.arcana.subtle);
      updateData.data['-=arcana'] = null;

      if(updateData.data.gnosis && updateData.data.wisdom) updateData.data.mage_traits = {gnosis: updateData.data.gnosis, wisdom: updateData.data.wisdom};
      updateData.data['-=gnosis'] = null;
      updateData.data['-=wisdom'] = null;

      if(updateData.data.disciplines?.common) updateData.data.disciplines_common = duplicate(updateData.data.disciplines.common);
      if(updateData.data.disciplines?.unique) updateData.data.disciplines_unique = duplicate(updateData.data.disciplines.unique);
      if(updateData.data.disciplines?.own) updateData.data.disciplines_own = duplicate(updateData.data.disciplines.own);
      updateData.data['-=disciplines'] = null;

      if(updateData.data.bloodPotency && updateData.data.humanity) updateData.data.vampire_traits = {bloodPotency: updateData.data.bloodPotency, humanity: updateData.data.humanity};
      updateData.data['-=bloodPotency'] = null;
      updateData.data['-=humanity'] = null;

      if(updateData.data.wyrd) updateData.data.changeling_traits = {wyrd: updateData.data.wyrd};
      updateData.data['-=wyrd'] = null;
    }

    if(actor.type === "ephemeral"){
      if(updateData.data.power) updateData.data.eph_physical = {power: duplicate(updateData.data.power)};
      if(updateData.data.finesse) updateData.data.eph_social = {finesse: duplicate(updateData.data.finesse)};
      if(updateData.data.resistance) updateData.data.eph_mental = {resistance: duplicate(updateData.data.resistance)};
      updateData.data['-=power'] = null;
      updateData.data['-=finesse'] = null;
      updateData.data['-=resistance'] = null;
    }

    if(updateData.data.sizeMod && updateData.data.speedMod && updateData.data.defenseMod && updateData.data.armorMod && updateData.data.initiativeModMod && updateData.data.ballisticMod && updateData.data.perceptionMod){ 
      updateData.data.derivedTraits = {
        size: {value: 0, mod: updateData.data.sizeMod},
        speed: {value: 0, mod: updateData.data.speedMod},
        defense: {value: 0, mod: updateData.data.defenseMod},
        armor: {value: 0, mod: updateData.data.armorMod},
        initiativeMod: {value: 0, mod: updateData.data.initiativeModMod},
        ballistic: {value: 0, mod: updateData.data.ballisticMod},
        perception: {value: 0, mod: updateData.data.perceptionMod}
      };

      updateData.data['-=size'] = null;
      updateData.data['-=speed'] = null;
      updateData.data['-=defense'] = null;
      updateData.data['-=armor'] = null;
      updateData.data['-=initiativeMod'] = null;
      updateData.data['-=ballistic'] = null;
      updateData.data['-=perception'] = null;

      updateData.data['-=sizeMod'] = null;
      updateData.data['-=speedMod'] = null;
      updateData.data['-=defenseMod'] = null;
      updateData.data['-=armorMod'] = null;
      updateData.data['-=initiativeModMod'] = null;
      updateData.data['-=ballisticMod'] = null;
      updateData.data['-=perceptionMod'] = null;
    }
  }

  // Migrate Owned Items
  if ( !actor.items ) return updateData;
  let hasItemUpdates = false;
  const items = actor.items.map(i => {

    // Migrate the Owned Item
    let itemUpdate = migrateItemData(i, version_nums_current, version_nums_migration, actor);

    // Update the Owned Item
    if ( !isObjectEmpty(itemUpdate) ) {
      hasItemUpdates = true;
      if(compareVersion(version_nums_current, [0,6,0]) && actor.type === "ephemeral") { // 0.5.0 -> 0.6.0
        if(i.type === "merit"){
          itemUpdate.type = "numen";
          itemUpdate.data.reaching = false;
        }
        else if(i.type === "formAbility"){
          itemUpdate.type = "manifestation";
        }
      }
      return mergeObject(i, itemUpdate, {insertKeys: true, insertValues: true, overwrite: true, enforceTypes: false, inplace: false});
    } else return i;
  });
  if ( hasItemUpdates ) updateData.items = items;

  return updateData;
};

/* -------------------------------------------- */

/**
 * Migrate a single Item entity to incorporate latest data model changes
 * @param item
 */
export const migrateItemData = function(item, version_nums_current, version_nums_migration) {
  const updateData = {};
  const model = game.system.model.Item[item.type];
  
  updateData.data = _nullToUndefined(item.data, 5);
  updateData.data = mergeObject(updateData.data,model,{insertKeys: true, insertValues: true, overwrite: false, inplace: false, enforceTypes: false})

  if(compareVersion(version_nums_current, [0,3,0]) && item.type === "melee" && !updateData.data.weaponType) updateData.data.weaponType = "Melee"; // 0.2.3 -> 0.3.0

  if(compareVersion(version_nums_current, [0,6,0])) { // 0.5.0 -> 0.6.0
    if(updateData.data.arcanum){
      if(updateData.data.arcanum === "Forces") updateData.data.arcanum = "forces";
      else if(updateData.data.arcanum === "Life") updateData.data.arcanum = "life";
      else if(updateData.data.arcanum === "Matter") updateData.data.arcanum = "matter";
      else if(updateData.data.arcanum === "Space") updateData.data.arcanum = "space";
      else if(updateData.data.arcanum === "Time") updateData.data.arcanum = "time";
      else if(updateData.data.arcanum === "Death") updateData.data.arcanum = "death";
      else if(updateData.data.arcanum === "Fate") updateData.data.arcanum = "fate";
      else if(updateData.data.arcanum === "Mind") updateData.data.arcanum = "mind";
      else if(updateData.data.arcanum === "Prime") updateData.data.arcanum = "prime";
      else if(updateData.data.arcanum === "Spirit") updateData.data.arcanum = "spirit";
    }
    if(updateData.data.effects) updateData.data.effects = [];
  }

  return updateData;
};

/* -------------------------------------------- */

/**
 * Migrate a single Scene entity to incorporate changes to the data model of it's actor data overrides
 * Return an Object of updateData to be applied
 * @param {Object} scene  The Scene data to Update
 * @return {Object}       The updateData to apply
 */
 /* export const migrateSceneData = function(scene, version_nums_current, version_nums_migration) {
  const tokens = duplicate(scene.tokens);
  return {
    tokens: tokens.map(t => {
      if (!t.actorId || t.actorLink || !t.actorData.data) {
        t.actorData = {};
        return t;
      }
      const token = new Token(t);
      if ( !token.actor ) {
        t.actorId = null;
        t.actorData = {};
      } else if ( !t.actorLink ) {
        const updateData = migrateActorData(token.data.actorData, version_nums_current, version_nums_migration);
        t.actorData = mergeObject(token.data.actorData, updateData);
      }
      return t;
    })
  };
}; */



export const migrateSceneData = function(scene, version_nums_current, version_nums_migration) {
  const tokens = scene.tokens.map(t => {
    //const t = token.toJSON();
    if (!t.actorId || t.actorLink) {
      t.actorData = {};
    }
    else if ( !game.actors.has(t.actorId) ){
      t.actorId = null;
      t.actorData = {};
    }
    else if ( !t.actorLink ) {
      const actorData = duplicate(t.actorData);
      actorData.type = token.actor?.type;
      const update = migrateActorData(actorData, version_nums_current, version_nums_migration);

      mergeObject(t.actorData, update);
    }
    return t;
  });
  return {tokens};
};


/* -------------------------------------------- */
/*  Low level migration utilities
/* -------------------------------------------- */

/**
 * Converts all properties with value null to value undefined,
 * in order to let the mergeObject function replace these values.
 * @private
 */
function _nullToUndefined(data, recDepth) {
  Object.entries(data).forEach(ele => {
    if(ele[1] === null){ data[ele[0]] = undefined;}
    else if((recDepth > 0) && (toString.call(ele[1]) == '[object Object]') && (Object.keys(ele[1]).length > 0)){
      data[ele[0]] = _nullToUndefined(ele[1], recDepth-1); 
    } 
  });
  return data;
}

// Returns true  if the current version is lower than the migration version
export const compareVersion = function(version_nums_current, version_nums_migration) {
  return (version_nums_current[0] < version_nums_migration[0]) || (version_nums_current[0] === version_nums_migration[0] && version_nums_current[1] < version_nums_migration[1]) || (version_nums_current[0] === version_nums_migration[0] && version_nums_current[1] === version_nums_migration[1] && version_nums_current[2] < version_nums_migration[2]);
}